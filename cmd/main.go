package main

import (
	"example.com/m/v2/api"
	"example.com/m/v2/api/handlers"
	"example.com/m/v2/config"
	"example.com/m/v2/pkg/logger"
	"example.com/m/v2/services"

	"github.com/gin-gonic/gin"
)

func main() {
	cfg := config.Load()

	grpcSvcs, err := services.NewGrpcClients(cfg)
	if err != nil {
		panic(err)
	}
	var loggerLevel = new(string)
	*loggerLevel = logger.LevelDebug
	
	switch cfg.Environment {
	case config.DebugMode:
		*loggerLevel = logger.LevelDebug
		gin.SetMode(gin.DebugMode)
	case config.TestMode:
		*loggerLevel = logger.LevelDebug
		gin.SetMode(gin.TestMode)
	default:
		*loggerLevel = logger.LevelInfo
		gin.SetMode(gin.ReleaseMode)
	}

	log := logger.NewLogger("shans_go_api", *loggerLevel)
	defer func() {
		err := logger.Cleanup(log)
		if err != nil {
			return
		}
		}()
		
		r := gin.New()

		r.Use(gin.Logger(), gin.Recovery())
		
		h := handlers.NewHandler(cfg, log, grpcSvcs)
		
		api.SetUpAPI(r, h, cfg)
		
		if err := r.Run(cfg.HTTPPort); err != nil {
			return
		}
	}
	