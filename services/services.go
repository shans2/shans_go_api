package services

import (
	"example.com/m/v2/config"

	"example.com/m/v2/genproto/content_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	CategoryService() content_service.CategoryServiceClient
	DivisionService() content_service.DivisionServiceClient
	VacancyService() content_service.VacancyServiceClient
	ContactService() content_service.ContactServiceClient
	AttributeService() content_service.AttributeServiceClient
	NotificationService() content_service.NotificationServiceClient
}

type grpcClients struct {
	divisionService     content_service.DivisionServiceClient
	categoryService     content_service.CategoryServiceClient
	vacancyService      content_service.VacancyServiceClient
	contactService      content_service.ContactServiceClient
	attributeService    content_service.AttributeServiceClient
	notificationService content_service.NotificationServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	connContentService, err := grpc.Dial(
		cfg.ContentServiceHost+cfg.ContentGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		divisionService:     content_service.NewDivisionServiceClient(connContentService),
		categoryService:     content_service.NewCategoryServiceClient(connContentService),
		vacancyService:      content_service.NewVacancyServiceClient(connContentService),
		contactService:      content_service.NewContactServiceClient(connContentService),
		attributeService:    content_service.NewAttributeServiceClient(connContentService),
		notificationService: content_service.NewNotificationServiceClient(connContentService),
	}, nil
}

func (g *grpcClients) DivisionService() content_service.DivisionServiceClient {
	return g.divisionService
}

func (g *grpcClients) CategoryService() content_service.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) VacancyService() content_service.VacancyServiceClient {
	return g.vacancyService
}

func (g *grpcClients) ContactService() content_service.ContactServiceClient {
	return g.contactService
}

func (g *grpcClients) AttributeService() content_service.AttributeServiceClient {
	return g.attributeService
}

func (g *grpcClients) NotificationService() content_service.NotificationServiceClient {
	return g.notificationService
}
