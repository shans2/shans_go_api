package api

import (
	"example.com/m/v2/api/docs"
	"example.com/m/v2/api/handlers"
	"example.com/m/v2/config"

	"github.com/gin-gonic/gin"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @description This is a api gateway
// @termsOfService https://udevs.io
func SetUpAPI(r *gin.Engine, h handlers.Handler, cfg config.Config) {
	docs.SwaggerInfo.Title = cfg.ServiceName
	docs.SwaggerInfo.Version = cfg.Version
	// docs.SwaggerInfo.Host = cfg.ServiceHost + cfg.HTTPPort
	docs.SwaggerInfo.Schemes = []string{cfg.HTTPScheme}

	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(5000))

	r.GET("/ping", h.Ping)
	r.GET("/config", h.GetConfig)

	//upload

	v1 := r.Group("/v1")
	// @securityDefinitions.apikey ApiKeyAuth
	// @in header
	// @name Authorization
	// v1.Use(h.AuthMiddleware())
	{
		v1.POST("/upload", h.Upload)

		// CONTENT_SERVICE

		//object-builder
		// v1.POST("/branch", h.CreateBranch)
		// v1.GET("/branch/:branch_id", h.GetSingleBranch)
		// v1.GET("/branch", h.GetBranchesList)
		// v1.PUT("/branch", h.UpdateBranch)
		// v1.DELETE("/branch/:branch_id", h.DeleteBranch)

		v1.POST("/division", h.CreateDivision)
		v1.GET("/division/:division_id", h.GetSingleDivision)
		v1.GET("/division", h.GetDivisionsList)
		v1.PUT("/division", h.UpdateDivision)
		v1.DELETE("/division/:division_id", h.DeleteDivision)

		v1.POST("/category", h.CreateCategory)
		v1.GET("/category/:category_id", h.GetSingleCategory)
		v1.GET("/category", h.GetCategoriesList)
		v1.PUT("/category", h.UpdateCategory)
		v1.DELETE("/category/:category_id", h.DeleteCategory)

		// v1.POST("/subcategory", h.CreateSubcategory)
		// v1.GET("/subcategory/:subcategory_id", h.GetSingleSubcategory)
		// v1.GET("/subcategory", h.GetSubcategoriesList)
		// v1.PUT("/subcategory", h.UpdateSubcategory)
		// v1.DELETE("/subcategory/:subcategory_id", h.DeleteSubcategory)

		// v1.POST("/brand", h.CreateBrand)
		// v1.GET("/brand/:brand_id", h.GetSingleBrand)
		// v1.GET("/brand", h.GetBrandsList)
		// v1.PUT("/brand", h.UpdateBrand)
		// v1.DELETE("/brand/:brand_id", h.DeleteBrand)

		// v1.POST("/attribute", h.CreateAttribute)
		// v1.GET("/attribute/:attribute_id", h.GetSingleAttribute)
		// v1.GET("/attribute", h.GetAttributesList)
		// v1.PUT("/attribute", h.UpdateAttribute)
		// v1.DELETE("/attribute/:attribute_id", h.DeleteAttribute)

		// v1.POST("/video", h.CreateVideo)
		// v1.GET("/video/:video_id", h.GetSingleVideo)
		// v1.GET("/video", h.GetVideosList)
		// v1.PUT("/video", h.UpdateVideo)
		// v1.DELETE("/video/:video_id", h.DeleteVideo)

		// v1.POST("/delivery_measure", h.CreateDeliveryMeasure)
		// v1.GET("/delivery_measure/:delivery_measure_id", h.GetSingleDeliveryMeasure)
		// v1.GET("/delivery_measure", h.GetDeliveryMeasuresList)
		// v1.PUT("/delivery_measure", h.UpdateDeliveryMeasure)
		// v1.DELETE("/delivery_measure/:delivery_measure_id", h.DeleteDeliveryMeasure)

		// v1.POST("/sticker", h.CreateSticker)
		// v1.GET("/sticker/:sticker_id", h.GetSingleSticker)
		// v1.GET("/sticker", h.GetStickersList)
		// v1.PUT("/sticker", h.UpdateSticker)
		// v1.DELETE("/sticker/:sticker_id", h.DeleteSticker)

		// v1.POST("/banner", h.CreateBanner)
		// v1.GET("/banner/:banner_id", h.GetSingleBanner)
		// v1.GET("/banner", h.GetBannersList)
		// v1.PUT("/banner", h.UpdateBanner)
		// v1.DELETE("/banner/:banner_id", h.DeleteBanner)

		// v1.POST("/discount_banner", h.CreateDiscountBanner)
		// v1.GET("/discount_banner/:discount_banner_id", h.GetSingleDiscountBanner)
		// v1.GET("/discount_banner", h.GetDiscountBannersList)
		// v1.PUT("/discount_banner", h.UpdateDiscountBanner)
		// v1.DELETE("/discount_banner/:discount_banner_id", h.DeleteDiscountBanner)

		// v1.POST("/voucher", h.CreateVoucher)
		// v1.GET("/voucher/:voucher_id", h.GetSingleVoucher)
		// v1.GET("/voucher", h.GetVouchersList)
		// v1.PUT("/voucher", h.UpdateVoucher)
		// v1.DELETE("/voucher/:voucher_id", h.DeleteVoucher)

		// v1.POST("/ribbon", h.CreateRibbon)
		// v1.GET("/ribbon/:ribbon_id", h.GetSingleRibbon)
		// v1.GET("/ribbon", h.GetRibbonsList)
		// v1.PUT("/ribbon", h.UpdateRibbon)
		// v1.DELETE("/ribbon/:ribbon_id", h.DeleteRibbon)

		// v1.POST("/discount", h.CreateDiscount)
		// v1.GET("/discount/:discount_id", h.GetSingleDiscount)
		// v1.GET("/discount", h.GetDiscountsList)
		// v1.PUT("/discount", h.UpdateDiscount)
		// v1.DELETE("/discount/:discount_id", h.DeleteDiscount)

		// v1.POST("/group", h.CreateGroup)
		// v1.GET("/group/:group_id", h.GetSingleGroup)
		// v1.GET("/group", h.GetGroupsList)
		// v1.PUT("/group", h.UpdateGroup)
		// v1.DELETE("/group/:group_id", h.DeleteGroup)

		// v1.POST("/promotion", h.CreatePromotion)
		// v1.GET("/promotion/:promotion_id", h.GetSinglePromotion)
		// v1.GET("/promotion", h.GetPromotionsList)
		// v1.PUT("/promotion", h.UpdatePromotion)
		// v1.DELETE("/promotion/:promotion_id", h.DeletePromotion)

		v1.POST("/contact", h.CreateContact)
		v1.GET("/contact/:contact_id", h.GetSingleContact)
		v1.GET("/contact", h.GetContactList)
		v1.PUT("/contact", h.UpdateContact)
		v1.DELETE("/contact/:contact_id", h.DeleteContact)

		v1.POST("/vacancy", h.CreateVacancy)
		v1.GET("/vacancy/:vacancy_id", h.GetSingleVacancy)
		v1.GET("/vacancy", h.GetVacancyList)
		v1.PUT("/vacancy", h.UpdateVacancy)
		v1.DELETE("/vacancy/:vacancy_id", h.Deletevacancy)

		v1.POST("/attribute", h.CreateAttribute)
		v1.GET("/attribute/:attribute_id", h.GetSingleAttribute)
		v1.GET("/attribute", h.GetAttributeList)
		v1.PUT("/attribute", h.UpdateAttribute)
		v1.DELETE("/attribute/:attribute_id", h.DeleteAttribute)

		v1.POST("/notification", h.CreateNotification)
		v1.GET("/notification/:notification_id", h.GetSingleNotification)
		v1.GET("/notification", h.GetNotificationsList)
		v1.PUT("/notification", h.UpdateNotification)
		v1.DELETE("/notification/:notification_id", h.DeleteNotification)

		// 1C CRONJOB

		// v1.POST("/import_products", h.ImportProducts)

	}

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE")
		c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}
