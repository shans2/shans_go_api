package handlers

import (
	"context"

	"example.com/m/v2/api/http"
	cs "example.com/m/v2/genproto/content_service"
	"example.com/m/v2/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateNotification godoc
// @Security ApiKeyAuth
// @ID create_notification
// @Router /v1/notification [POST]
// @Summary Create notification
// @Description Create notification
// @Tags Notification
// @Accept json
// @Produce json
// @Param Notification body content_service.CreateNotificationRequest true "CreateNotificationRequestBody"
// @Success 201 {object} http.Response{data=content_service.Notification} "Notification data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateNotification(c *gin.Context) {
	var notification cs.CreateNotificationRequest

	err := c.ShouldBindJSON(&notification)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.NotificationService().Create(
		context.Background(),
		&notification,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}


// GetSingleNotification godoc
// @Security ApiKeyAuth
// @ID get_notification_by_id
// @Router /v1/notification/{notification_id} [GET]
// @Summary Get single Notification
// @Description Get single Notification
// @Tags Notification
// @Accept json
// @Produce json
// @Param notification_id path string true "notification_id"
// @Success 200 {object} http.Response{data=content_service.Notification} "NotificationBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSingleNotification(c *gin.Context) {
	notificationID := c.Param("notification_id")

	if !util.IsValidUUID(notificationID) {
		h.handleResponse(c, http.InvalidArgument, "notification id is an invalid uuid")
		return
	}
	resp, err := h.services.NotificationService().GetSingle(
		context.Background(),
		&cs.NotificationPrimaryKey{
			Id: notificationID,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}


// UpdateNotification godoc
// @Security ApiKeyAuth
// @ID update_notification
// @Router /v1/notification [PUT]
// @Summary Update Notification
// @Description Update Notification
// @Tags Notification
// @Accept json
// @Produce json
// @Param Notification body content_service.Notification true "UpdateNotificationRequestBody"
// @Success 200 {object} http.Response{data=content_service.Notification} "Notification data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateNotification(c *gin.Context) {
	var notification cs.Notification

	err := c.ShouldBindJSON(&notification)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}
	resp, err := h.services.NotificationService().Update(
		context.Background(),
		&notification,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}



// DeleteNotification godoc
// @Security ApiKeyAuth
// @ID delete_notification
// @Router /v1/notification/{notification_id} [DELETE]
// @Summary Delete notification
// @Description Delete Notification
// @Tags Notification
// @Accept json
// @Produce json
// @Param notification_id path string true "notification_id"
// @Success 204
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteNotification(c *gin.Context) {
	notificationID := c.Param("notification_id")

	if !util.IsValidUUID(notificationID) {
		h.handleResponse(c, http.InvalidArgument, "notification id is an invalid uuid")
		return
	}

	resp, err := h.services.NotificationService().Delete(
		context.Background(),
		&cs.NotificationPrimaryKey{
			Id: notificationID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}


// GetNotificationLists godoc
// @Security ApiKeyAuth
// @ID get_notification_list
// @Router /v1/notification [GET]
// @Summary Get Notification list
// @Description Get Notification list
// @Tags Notification
// @Accept json
// @Produce json
// @Param filters query content_service.GetNotificationsListRequest true "filters"
// @Success 200 {object} http.Response{data=content_service.GetNotificationsListResponse} "NotificationBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetNotificationsList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.NotificationService().GetList(
		context.Background(),
		&cs.GetNotificationsListRequest{
			Limit:  int32(limit),
			Offset: int32(offset),
			Name: c.DefaultQuery("name", ""),
			Status: c.DefaultQuery("status", ""),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}