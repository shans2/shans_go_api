package handlers

import (
	"context"

	"example.com/m/v2/api/http"
	cs "example.com/m/v2/genproto/content_service"
	"example.com/m/v2/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateDivision godoc
// @Security ApiKeyAuth
// @ID create_division
// @Router /v1/division [POST]
// @Summary Create division
// @Description Create division
// @Tags Division
// @Accept json
// @Produce json
// @Param Division body content_service.CreateDivisionRequest true "CreateDivisionRequestBody"
// @Success 201 {object} http.Response{data=content_service.Division} "Division data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateDivision(c *gin.Context) {
	var division cs.CreateDivisionRequest

	err := c.ShouldBindJSON(&division)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.DivisionService().Create(
		context.Background(),
		&division,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetSingleDivision godoc
// @Security ApiKeyAuth
// @ID get_division_by_id
// @Router /v1/division/{division_id} [GET]
// @Summary Get single division
// @Description Get single division
// @Tags Division
// @Accept json
// @Produce json
// @Param division_id path string true "division_id"
// @Success 200 {object} http.Response{data=content_service.Division} "DivisionBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSingleDivision(c *gin.Context) {
	divisionID := c.Param("division_id")

	if !util.IsValidUUID(divisionID) {
		h.handleResponse(c, http.InvalidArgument, "division id is an invalid uuid")
		return
	}
	resp, err := h.services.DivisionService().GetSingle(
		context.Background(),
		&cs.DivisionPrimaryKey{
			Id: divisionID,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateDivision godoc
// @Security ApiKeyAuth
// @ID update_division
// @Router /v1/division [PUT]
// @Summary Update division
// @Description Update division
// @Tags Division
// @Accept json
// @Produce json
// @Param Division body content_service.Division true "UpdateDivisionRequestBody"
// @Success 200 {object} http.Response{data=content_service.Division} "Division data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateDivision(c *gin.Context) {
	var division cs.Division

	err := c.ShouldBindJSON(&division)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}
	resp, err := h.services.DivisionService().Update(
		context.Background(),
		&division,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteDivision godoc
// @Security ApiKeyAuth
// @ID delete_division
// @Router /v1/division/{division_id} [DELETE]
// @Summary Delete division
// @Description Delete division
// @Tags Division
// @Accept json
// @Produce json
// @Param division_id path string true "division_id"
// @Success 204
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteDivision(c *gin.Context) {
	DivisionID := c.Param("division_id")

	if !util.IsValidUUID(DivisionID) {
		h.handleResponse(c, http.InvalidArgument, "division id is an invalid uuid")
		return
	}

	resp, err := h.services.DivisionService().Delete(
		context.Background(),
		&cs.DivisionPrimaryKey{
			Id: DivisionID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}

// GetDivisionLists godoc
// @Security ApiKeyAuth
// @ID get_division_list
// @Router /v1/division [GET]
// @Summary Get Division list
// @Description Get Division list
// @Tags Division
// @Accept json
// @Produce json
// @Param filters query content_service.GetDivisionsListRequest true "filters"
// @Success 200 {object} http.Response{data=content_service.GetDivisionsListResponse} "DivisionBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetDivisionsList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.DivisionService().GetList(
		context.Background(),
		&cs.GetDivisionsListRequest{
			Limit:  int32(limit),
			Offset: int32(offset),
			Search: c.DefaultQuery("search", ""),
			Status: c.DefaultQuery("status", ""),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}
