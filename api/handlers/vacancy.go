package handlers

import (
	"context"

	"example.com/m/v2/api/http"
	cs "example.com/m/v2/genproto/content_service"
	"example.com/m/v2/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateVacancy godoc
// @Security ApiKeyAuth
// @ID create_vacancy
// @Router /v1/vacancy [POST]
// @Summary Create Vacancy
// @Description Create Vacancy
// @Tags Vacancy
// @Accept json
// @Produce json
// @Param Vacancy body content_service.CreateVacancyRequest true "CreateVacancyRequestBody"
// @Success 201 {object} http.Response{data=content_service.Vacancy} "Vacancy data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateVacancy(c *gin.Context) {
	var vacancy cs.CreateVacancyRequest

	err := c.ShouldBindJSON(&vacancy)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.VacancyService().Create(
		context.Background(),
		&vacancy,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}


// GetSingleVacancy godoc
// @Security ApiKeyAuth
// @ID get_vacancy_by_id
// @Router /v1/vacancy/{vacancy_id} [GET]
// @Summary Get single vacancy
// @Description Get single vacancy
// @Tags Vacancy
// @Accept json
// @Produce json
// @Param vacancy_id path string true "vacancy_id"
// @Success 200 {object} http.Response{data=content_service.Vacancy} "VacancyBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSingleVacancy(c *gin.Context) {
	vacancyID := c.Param("vacancy_id")

	if !util.IsValidUUID(vacancyID) {
		h.handleResponse(c, http.InvalidArgument, "vacancy id is an invalid uuid")
		return
	}
	resp, err := h.services.VacancyService().GetSingle(
		context.Background(),
		&cs.VacancyPrimaryKey{
			Id: vacancyID,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}


// UpdateVacancy godoc
// @Security ApiKeyAuth
// @ID update_vacancy
// @Router /v1/vacancy [PUT]
// @Summary Update vacancy
// @Description Update vacancy
// @Tags Vacancy
// @Accept json
// @Produce json
// @Param Vacancy body content_service.Vacancy true "UpdateVacancyRequestBody"
// @Success 200 {object} http.Response{data=content_service.Vacancy} "Vacancy data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateVacancy(c *gin.Context) {
	var vacancy cs.Vacancy

	err := c.ShouldBindJSON(&vacancy)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}
	resp, err := h.services.VacancyService().Update(
		context.Background(),
		&vacancy,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}


// DeleteVacancy godoc
// @Security ApiKeyAuth
// @ID	delete_vacancy
// @Router /v1/vacancy/{vacancy_id} [DELETE]
// @Summary Delete vacancy
// @Description Delete vacancy
// @Tags Vacancy
// @Accept json
// @Produce json
// @Param vacancy_id path string true "vacancy_id"
// @Success 204
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) Deletevacancy(c *gin.Context) {
	vacancyID := c.Param("vacancy_id")

	if !util.IsValidUUID(vacancyID) {
		h.handleResponse(c, http.InvalidArgument, "vacancy id is an invalid uuid")
		return
	}

	resp, err := h.services.VacancyService().Delete(
		context.Background(),
		&cs.VacancyPrimaryKey{
			Id: vacancyID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}

// GetVacancyLists godoc
// @Security ApiKeyAuth
// @ID get_vacancy_list
// @Router /v1/vacancy [GET]
// @Summary Get Vacancy list
// @Description Get Vacancy list
// @Tags Vacancy
// @Accept json
// @Produce json
// @Param filters query content_service.GetVacancysListRequest true "filters"
// @Success 200 {object} http.Response{data=content_service.GetVacancysListResponse} "VacancyBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetVacancyList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.VacancyService().GetList(
		context.Background(),
		&cs.GetVacancysListRequest{
			Limit:  int32(limit),
			Offset: int32(offset),
			Status: c.DefaultQuery("status", ""),
			Name:   c.DefaultQuery("name", ""),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

