package handlers

import (
	"context"
	"strconv"

	"example.com/m/v2/api/http"
	cs "example.com/m/v2/genproto/content_service"
	"example.com/m/v2/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateCategory godoc
// @Security ApiKeyAuth
// @ID create_category
// @Router /v1/category [POST]
// @Summary Create category
// @Description Create category
// @Tags Category
// @Accept json
// @Produce json
// @Param Category body content_service.CreateCategoryRequest true "CreateCategoryRequestBody"
// @Success 201 {object} http.Response{data=content_service.Category} "Category data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateCategory(c *gin.Context) {
	var category cs.CreateCategoryRequest

	err := c.ShouldBindJSON(&category)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.CategoryService().Create(
		context.Background(),
		&category,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetSingleCategory godoc
// @Security ApiKeyAuth
// @ID get_category_by_id
// @Router /v1/category/{category_id} [GET]
// @Summary Get single Category
// @Description Get single Category
// @Tags Category
// @Accept json
// @Produce json
// @Param category_id path string true "category_id"
// @Success 200 {object} http.Response{data=content_service.Category} "CategoryBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSingleCategory(c *gin.Context) {
	categoryID := c.Param("category_id")

	if !util.IsValidUUID(categoryID) {
		h.handleResponse(c, http.InvalidArgument, "category id is an invalid uuid")
		return
	}
	resp, err := h.services.CategoryService().GetSingle(
		context.Background(),
		&cs.CategoryPrimaryKey{
			Id: categoryID,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateCategory godoc
// @Security ApiKeyAuth
// @ID update_category
// @Router /v1/category [PUT]
// @Summary Update Category
// @Description Update Category
// @Tags Category
// @Accept json
// @Produce json
// @Param Category body content_service.UpdateCategoryRequest true "UpdateCategoryRequestBody"
// @Success 200 {object} http.Response{data=content_service.UpdateCategoryRequest} "Category data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateCategory(c *gin.Context) {
	var category cs.UpdateCategoryRequest

	err := c.ShouldBindJSON(&category)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}
	resp, err := h.services.CategoryService().Update(
		context.Background(),
		&category,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteCategory godoc
// @Security ApiKeyAuth
// @ID delete_category
// @Router /v1/category/{category_id} [DELETE]
// @Summary Delete Category
// @Description Delete Category
// @Tags Category
// @Accept json
// @Produce json
// @Param category_id path string true "category_id"
// @Success 204
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteCategory(c *gin.Context) {
	categoryID := c.Param("category_id")

	if !util.IsValidUUID(categoryID) {
		h.handleResponse(c, http.InvalidArgument, "category id is an invalid uuid")
		return
	}

	resp, err := h.services.CategoryService().Delete(
		context.Background(),
		&cs.CategoryPrimaryKey{
			Id: categoryID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}

// GetCategoriesList godoc
// @Security ApiKeyAuth
// @ID get_category_list
// @Router /v1/category [GET]
// @Summary Get Category list
// @Description Get Category list
// @Tags Category
// @Accept json
// @Produce json
// @Param filters query content_service.GetCategoriesListRequest true "filters"
// @Success 200 {object} http.Response{data=content_service.GetCategoriesListResponse} "CategoryBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCategoriesList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	order, err := strconv.Atoi(c.DefaultQuery("order", ""))

	resp, err := h.services.CategoryService().GetList(
		context.Background(),
		&cs.GetCategoriesListRequest{
			Limit:      int32(limit),
			Offset:     int32(offset),
			Name:       c.DefaultQuery("name", ""),
			DivisionId: c.DefaultQuery("division_id", ""),
			Order:      int32(order),
			Status:     c.DefaultQuery("status", ""),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}
