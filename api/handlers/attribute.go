package handlers

import (
	"context"

	"example.com/m/v2/api/http"
	cs "example.com/m/v2/genproto/content_service"
	"example.com/m/v2/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateAttribute godoc
// @Security ApiKeyAuth
// @ID create_attribute
// @Router /v1/attribute [POST]
// @Summary Create Attribute
// @Description Create Attribute
// @Tags Attribute
// @Accept json
// @Produce json
// @Param Attribute body content_service.CreateAttributeRequest true "CreateAttributeRequestBody"
// @Success 201 {object} http.Response{data=content_service.Attribute} "Attribute data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateAttribute(c *gin.Context) {
	var attribute cs.CreateAttributeRequest

	err := c.ShouldBindJSON(&attribute)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.AttributeService().Create(
		context.Background(),
		&attribute,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}


// GetSingleAttribute godoc
// @Security ApiKeyAuth
// @ID get_attribute_by_id
// @Router /v1/attribute/{attribute_id} [GET]
// @Summary Get single attribute
// @Description Get single attribute
// @Tags Attribute
// @Accept json
// @Produce json
// @Param attribute_id path string true "attribute_id"
// @Success 200 {object} http.Response{data=content_service.Attribute} "AttributeBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSingleAttribute(c *gin.Context) {
	attributeID := c.Param("attribute_id")

	if !util.IsValidUUID(attributeID) {
		h.handleResponse(c, http.InvalidArgument, "Attribute id is an invalid uuid")
		return
	}
	resp, err := h.services.AttributeService().GetSingle(
		context.Background(),
		&cs.AttributePrimaryKey{
			Id: attributeID,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}


// UpdateAttribute godoc
// @Security ApiKeyAuth
// @ID update_attribute
// @Router /v1/attribute [PUT]
// @Summary Update attribute
// @Description Update attribute
// @Tags Attribute
// @Accept json
// @Produce json
// @Param Attribute body content_service.Attribute true "UpdateAttributeRequestBody"
// @Success 200 {object} http.Response{data=content_service.Attribute} "Attribute data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateAttribute(c *gin.Context) {
	var attribute cs.Attribute

	err := c.ShouldBindJSON(&attribute)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}
	resp, err := h.services.AttributeService().Update(
		context.Background(),
		&attribute,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}


// DeleteAttribute godoc
// @Security ApiKeyAuth
// @ID	delete_attribute
// @Router /v1/attribute/{attribute_id} [DELETE]
// @Summary Delete attribute
// @Description Delete attribute
// @Tags Attribute
// @Accept json
// @Produce json
// @Param attribute_id path string true "attribute_id"
// @Success 204
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteAttribute(c *gin.Context) {
	attributeID := c.Param("attribute_id")

	if !util.IsValidUUID(attributeID) {
		h.handleResponse(c, http.InvalidArgument, "Attribute id is an invalid uuid")
		return
	}

	resp, err := h.services.AttributeService().Delete(
		context.Background(),
		&cs.AttributePrimaryKey{
			Id: attributeID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}

// GetAttributeLists godoc
// @Security ApiKeyAuth
// @ID get_attribute_list
// @Router /v1/attribute [GET]
// @Summary Get Attribute list
// @Description Get Attribute list
// @Tags Attribute
// @Accept json
// @Produce json
// @Param filters query content_service.GetAttributesListRequest true "filters"
// @Success 200 {object} http.Response{data=content_service.GetAttributesListResponse} "AttributeBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetAttributeList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.AttributeService().GetList(
		context.Background(),
		&cs.GetAttributesListRequest{
			Limit:  int32(limit),
			Offset: int32(offset),
			Status: c.DefaultQuery("status", ""),
			Name:   c.DefaultQuery("name", ""),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}