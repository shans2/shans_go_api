package handlers

import (
	"context"

	"example.com/m/v2/api/http"
	cs "example.com/m/v2/genproto/content_service"
	"example.com/m/v2/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateContact godoc
// @Security ApiKeyAuth
// @ID create_contact
// @Router /v1/contact [POST]
// @Summary Create Contact
// @Description Create Contact
// @Tags Contact
// @Accept json
// @Produce json
// @Param Contact body content_service.CreateContactRequest true "CreateContactRequestBody"
// @Success 201 {object} http.Response{data=content_service.Contact} "Contact data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateContact(c *gin.Context) {
	var contact cs.CreateContactRequest

	err := c.ShouldBindJSON(&contact)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ContactService().Create(
		context.Background(),
		&contact,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetSingleContact godoc
// @Security ApiKeyAuth
// @ID get_contact_by_id
// @Router /v1/contact/{contact_id} [GET]
// @Summary Get single contact
// @Description Get single contact
// @Tags Contact
// @Accept json
// @Produce json
// @Param contact_id path string true "contact_id"
// @Success 200 {object} http.Response{data=content_service.Contact} "ContactBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSingleContact(c *gin.Context) {
	contactID := c.Param("contact_id")

	if !util.IsValidUUID(contactID) {
		h.handleResponse(c, http.InvalidArgument, "contact id is an invalid uuid")
		return
	}
	resp, err := h.services.ContactService().GetSingle(
		context.Background(),
		&cs.ContactPrimaryKey{
			Id: contactID,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateContact godoc
// @Security ApiKeyAuth
// @ID update_contact
// @Router /v1/contact [PUT]
// @Summary Update contact
// @Description Update contact
// @Tags Contact
// @Accept json
// @Produce json
// @Param Contact body content_service.Contact true "UpdateContactRequestBody"
// @Success 200 {object} http.Response{data=content_service.Contact} "Contact data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateContact(c *gin.Context) {
	var contact cs.Contact

	err := c.ShouldBindJSON(&contact)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}
	resp, err := h.services.ContactService().Update(
		context.Background(),
		&contact,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteContact godoc
// @Security ApiKeyAuth
// @ID	delete_contact
// @Router /v1/contact/{contact_id} [DELETE]
// @Summary Delete contact
// @Description Delete contact
// @Tags Contact
// @Accept json
// @Produce json
// @Param contact_id path string true "contact_id"
// @Success 204
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteContact(c *gin.Context) {
	ContactID := c.Param("contact_id")

	if !util.IsValidUUID(ContactID) {
		h.handleResponse(c, http.InvalidArgument, "contact id is an invalid uuid")
		return
	}

	resp, err := h.services.ContactService().Delete(
		context.Background(),
		&cs.ContactPrimaryKey{
			Id: ContactID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}

// GetContactLists godoc
// @Security ApiKeyAuth
// @ID get_contact_list
// @Router /v1/contact [GET]
// @Summary Get Contact list
// @Description Get Contact list
// @Tags Contact
// @Accept json
// @Produce json
// @Param filters query content_service.GetContactsListRequest true "filters"
// @Success 200 {object} http.Response{data=content_service.GetContactsListResponse} "ContactBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetContactList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ContactService().GetList(
		context.Background(),
		&cs.GetContactsListRequest{
			Limit:  int32(limit),
			Offset: int32(offset),
			Status: c.DefaultQuery("status", ""),
			Type:   c.DefaultQuery("type", ""),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}
