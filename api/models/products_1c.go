package models

type Product1C struct {
	Products []ProductData `json:"products"`
}

type ProductData struct {
	Data Product `json:"data"`
}

type Product struct {
	Product     string   `json:"Product"`
	ProductName string   `json:"ProductName"`
	Price       string   `json:"Price"`
	CorpPrice   string   `json:"CorpPrice"`
	Details     []Detail `json:"Details"`
}

type Detail struct {
	Store          string `json:"Store"`
	ClosingBalance string `json:"ClosingBalance"`
}
